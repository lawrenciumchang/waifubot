const Discord = require ('discord.js');
const fetch = require('node-fetch');
const { quoteBaseUrl } = require('../config.json');
const { reply, sendEmbed } = require('../utility');

module.exports = {
  name: 'quote',
  async execute (client, interaction) {
    const response = await fetch(quoteBaseUrl + '/random');
    const data = await response.json();
    const quoteEmbed = new Discord.MessageEmbed()
      .setColor('#0099FF')
      .setDescription('"' + data.quote + '" \n\n' + data.character + ', ' + data.anime);
    reply(client, interaction, 'Fetching quote...');
    sendEmbed(client, interaction, quoteEmbed);
  }
};
