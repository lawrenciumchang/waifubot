const Discord = require ('discord.js');
const anilist = require('anilist-node');
const Anilist = new anilist();
const fetch = require('node-fetch');
const { reply, sendEmbed } = require('../utility');

module.exports = {
  name: 'character',
  async execute (client, interaction) {
    // As of 7/11/21, there are 14929 characters in the AniList databse
    const upperCharacterRange = 14929;
    const lowerCharacterRange = 1;
    const randomNumber = Math.floor(Math.random() * (upperCharacterRange - lowerCharacterRange) + lowerCharacterRange);

    Anilist.people.character(randomNumber).then(data => {
      const englishName = data.name?.english;
      const japaneseName = data.name?.native;
      const title = japaneseName ? englishName + ' - ' + japaneseName : englishName;
      const image = data.image.large ? data.image.large : data.image.medium;
      const description = data.description ? data.description : '';
      const characterEmbed = new Discord.MessageEmbed()
        .setColor('#0099FF')
        .setTitle(title)
        .setURL(data.siteUrl)
        .setImage(image)
        .setDescription(description);
      reply(client, interaction ,'Grabbing character...');
      sendEmbed(client, interaction, characterEmbed);
  });
  }
};
