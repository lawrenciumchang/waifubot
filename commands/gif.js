const fetch = require('node-fetch');
const { waifuBaseUrl, categories } = require('../config.json');
const { reply } = require('../utility');

module.exports = {
  name: 'gif',
  async execute (client, interaction, args) {
    let category = '';
    if (args) category = args[0].value;

    sendWaifuGif = async (category) => {
      const url = waifuBaseUrl.concat('/sfw/' + category);
      const response = await fetch(url);
      const image = await response.json();
      reply(client, interaction, image.url);
    };

    if (!category) {
      // Random gif
      const randomCategory = categories.gifs[Math.floor(Math.random() * categories.gifs.length)];
      sendWaifuGif(randomCategory);
    } else {
      sendWaifuGif(category);
    }
  }
};
