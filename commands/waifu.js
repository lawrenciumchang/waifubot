const Discord = require ('discord.js');
const fetch = require('node-fetch');
const { waifuBaseUrl } = require('../config.json');
const { reply, sendImageAttachment } = require('../utility');

module.exports = {
	name: 'waifu',
	async execute (client, interaction, args) {
    let category = '';
    if (args) category = args[0].value;

    sendWaifu = async (category, isNSFW) => {
      const url = waifuBaseUrl.concat(isNSFW ? '/nsfw/' + category : '/sfw/' + category);
      const response = await fetch(url);
      const image = await response.json();

      if (isNSFW) {
        reply(client, interaction, 'Generating waifu...');
        sendImageAttachment(client, interaction, image.url, 'SPOILER_NSFW_WAIFU.jpg');
      } else {
        reply(client, interaction, image.url);
      }
    };

    if (!category) {
      sendWaifu('waifu', false);
    } else {
      switch (category) {
        case 'nsfw':
          sendWaifu('waifu', true);
          break;
        case 'nsfw-neko':
          sendWaifu('neko', true);
          break;
        default: 
          sendWaifu(category, false);
          break;
      }
    }
	}
};
