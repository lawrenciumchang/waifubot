const Discord = require ('discord.js');

reply = (client, interaction, content) => {
  client.api.interactions(interaction.id, interaction.token).callback.post({
    data: {
      type: 4,
      data: {
        content: content,
      }
    }
  });
};

sendImageAttachment = (client, interaction, attachment, name) => {
  new Discord.WebhookClient(client.user.id, interaction.token).send({
    files: [{
      attachment: attachment,
      name: name,
    }]
  });;
};

sendEmbed = (client, interaction, embedObject) => {
  new Discord.WebhookClient(client.user.id, interaction.token).send({
    embeds: [embedObject]
  });;
};

module.exports = {
  reply,
  sendImageAttachment,
  sendEmbed
};
