const fs = require('fs');
const { Client, Collection } = require('discord.js');
const { token } = require('./config.json');
const client = new Client();
client.commands = new Collection();

// Create command list
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	client.commands.set(command.name, command);
}

// On ready
client.on('ready', () => {
  console.log(`${client.user.tag} ready!`);
  client.user.setActivity('slash commands!', { type: 'LISTENING' });

  // Reply to slash commands
  client.ws.on('INTERACTION_CREATE', async interaction => {
    const slashCommand = interaction.data.name.toLowerCase();
    const command = client.commands.get(slashCommand);
    const args = interaction.data.options; // args: [ { value: 'neko', type: 3, name: 'category' } ]

    try {
      command.execute(client, interaction, args);
    } catch (error) {
      console.error('Error executing command --- ', error);
      client.api.interactions(interaction.id, interaction.token).callback.post({data: {
        type: 4,
        data: {
          content: 'There was an error trying to execute that command!'
        }
      }});
    }
  });
});

// Login to Discord with token
client.login(token);
