# WaifuBot

A Discord bot for waifus.

## List of commands

`npm install` to get project set up.

`node .` to start and run locally.

`nodemon .` to watch for local changes.

`pm2 start .` to run as process.

`pm2 stop .` to stop process.

`pm2 restart .` to restart process.

`pm2 list` to view list of processes.

`pm2 startup` and `pm2 save` to set and run processes on startup.
